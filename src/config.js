require('babel-polyfill');

const environment = {
  development: {
    isProduction: false
  },
  production: {
    isProduction: true
  }
}[process.env.NODE_ENV || 'development'];

module.exports = Object.assign({
  host: process.env.HOST || 'localhost',
  port: process.env.PORT,
  apiHost: process.env.APIHOST || 'localhost',
  apiPort: process.env.APIPORT,
  app: {
    title: 'Lab Data Warehouse',
    description: 'Stores all datasets available in the lab !',
    head: {
      titleTemplate: 'Lab Data Warehouse: %s',
      meta: [
        {name: 'description', content: 'Manage your lab data pile.'},
        {charset: 'utf-8'},
        {property: 'og:site_name', content: 'Furlong Lab'},
        {property: 'og:image', content: 'https://react-redux.herokuapp.com/logo.jpg'},
        {property: 'og:locale', content: 'en_US'},
        {property: 'og:title', content: 'Lab Data Warehouse'},
        {property: 'og:description', content: 'Manage your lab data pile easily.'},
        {property: 'og:card', content: 'summary'},
        {property: 'og:site', content: '@magiccharly'},
        {property: 'og:creator', content: '@magiccharly'},
        {property: 'og:image:width', content: '200'},
        {property: 'og:image:height', content: '200'}
      ]
    }
  },

}, environment);
