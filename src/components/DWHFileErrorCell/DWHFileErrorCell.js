import React, {Component, PropTypes} from 'react';

const codemap = {
  'ERROR_WRONG_LOCATION': 'Bad Location',
  'ERROR_MISSING_META': 'Missing Meta'
};

export default class DWHFileErrorCell extends Component {
  static propTypes = {
    data: PropTypes.array,
    rowData: PropTypes.object.isRequired
  }

  render() {
    const codes = this.props.data;
    const messages = this.props.rowData.error_descriptions;
    const buttons = [];
    for (let idx = 0; idx < codes.length; idx++) {
      buttons.push( <button key={'error_button_' + idx} type="button" className="btn-danger" title={messages[idx]}>{codemap[codes[idx]]}</button> );
    }
    // const styles = require('./InfoBar.scss');
    return (
      <div>
          { buttons }
      </div>
    );
  }
}
