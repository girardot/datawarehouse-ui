import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as dwhActions from 'redux/modules/dwh';


@connect(
  () => ({}),
  dispatch => bindActionCreators(dwhActions, dispatch)
)
@reduxForm({
  form: 'transferfiles',
  fields: [ 'clonepath', 'message', 'dryrun' ],
  initialValues: {'clonepath': '/path/to/default/clone/', 'dryrun': 'true'}
})
export default
class TransferFileForm extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    tranferFiles: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    filesAvailable: PropTypes.bool.isRequired
  }


  handleSubmit = (data) => {
    // console.log(JSON.stringify(data));
    this.props.tranferFiles(data);
  }


  render() {
    const {
      fields: { clonepath, message, dryrun },
      handleSubmit,
      filesAvailable
    } = this.props;

    const styles = require('./TransferFileForm.scss');
    const renderInput = (field, label) =>
      <div className={'form-group' + (field.error && field.touched ? ' has-error' : '')}>
        <label htmlFor={field.name} className="col-sm-2">{label}</label>
        <div className={'col-sm-8 ' + styles.inputGroup}>
          <input type="text" className="form-control" id={field.name} {...field}/>
          {field.error && field.touched && <div className="text-danger">{field.error}</div>}
        </div>
      </div>;

    return (
      <div>
        <h2> Position action to preform </h2>
        <form className="form-horizontal" onSubmit={handleSubmit(this.handleSubmit)}>
          {renderInput(clonepath, 'Clone Location')}
          {
          (filesAvailable ? renderInput(message, 'Commit Message') : <p></p>)
          }
          <div className="form-group">
            <label className="col-sm-2">Action</label>
            <div className="col-sm-8">
              <input type="radio" id="dryrun-true" {...dryrun} value="true" checked={dryrun.value === 'true'} />
              <label htmlFor="dryrun-true" className={styles.radioLabel}>Scan clone</label>
              <input type="radio" id="dryrun-false" {...dryrun} value="false" checked={dryrun.value === 'false'} disabled={filesAvailable === false}/>
              <label htmlFor="dryrun-false" className={styles.radioLabel}>Push Files</label>
            </div>
          </div>
          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <button className="btn btn-success" type="submit" > OK </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
