import React, {Component, PropTypes} from 'react';
import FontAwesome from 'react-fontawesome';


const codemap = {
  'STATUS_ERROR': 'Error',
  'STATUS_TRANSFERRED': 'Transferred',
  'STATUS_TRANSFERABLE': 'Transferable'
};


export default class DWHFileStatus extends Component {
  static propTypes = {
    data: PropTypes.string.isRequired,
    rowData: PropTypes.object.isRequired
  }

  render() {
    const status = this.props.data;
    let faName = '';
    let divStyle = {backgroundColor: 'purple'};
    let info = (status && codemap[status] ? codemap[status] : '');
    switch (status) {
      case 'STATUS_ERROR':
        faName = 'exclamation-circle';
        divStyle = {backgroundColor: 'red'};
        break;
      case 'STATUS_TRANSFERRED':
        faName = 'check-circle-o';
        divStyle = {backgroundColor: 'green'};
        break;
      case 'STATUS_TRANSFERABLE':
        faName = 'thumbs-o-up';
        divStyle = {backgroundColor: 'yellow'};
        break;
      default:
        faName = 'question-circle-o';
        info = 'unknown status of file: ' + status;
    }

    return (
      <div title={info} >
        <center><p style={divStyle} ><FontAwesome name={faName} /></p></center>
      </div>
    );
  }
}
