import React, {Component, PropTypes} from 'react';

export default class DWHFileExpFactorCell extends Component {
  static propTypes = {
    data: PropTypes.array,
    rowData: PropTypes.object.isRequired
  }

  render() {
    const factors = this.props.data;
    const values = this.props.rowData.factorvalues;
    console.log(factors);
    console.log(values);
    const spans = [];
    for (let idx = 0; idx < factors.length; idx++) {
      spans.push( <div key={'div_expfactor_' + idx} ><strong>{factors[idx] + ' : '}</strong>{values[idx]}</div> );
    }
    // const styles = require('./InfoBar.scss');
    return (
      <div>
          { spans }
      </div>
    );
  }
}
