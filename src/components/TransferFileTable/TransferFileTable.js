import React, {Component, PropTypes} from 'react';
import Griddle from 'griddle-react';
import DWHFileExpFactorCell from '../DWHFileExpFactorCell/DWHFileExpFactorCell';
import DWHFileErrorCell from '../DWHFileErrorCell/DWHFileErrorCell';
import DWHFileStatus from '../DWHFileStatus/DWHFileStatus';

export default class TransferFileTable extends Component {
  static propTypes = {
    dwhfiles: PropTypes.array.isRequired
  }

  render() {
    const {dwhfiles} = this.props;
    //  'factors': ['tissue', 'target', 'stage'],
    //  'factorvalues': ['we', 'Mef2', '8-10h'],
    const columnMetadata = [
      {
        'columnName': 'status',
        'order': 1,
        'locked': true,
        'visible': true,
        'customComponent': DWHFileStatus,
        'displayName': 'Status'
      },
      {
        'columnName': 'name',
        'order': 2,
        'locked': true,
        'visible': true,
        'displayName': 'File Name'
      },
      {
        'columnName': 'path',
        'order': 3,
        'locked': false,
        'visible': true,
        'displayName': 'File Name'
      },
      {
        'columnName': 'organism',
        'order': 4,
        'locked': false,
        'visible': true,
        'displayName': 'Organism'
      },
      {
        'columnName': 'technology',
        'order': 5,
        'locked': false,
        'visible': true,
        'displayName': 'Technology'
      },
      {
        'columnName': 'experiment_type',
        'order': 6,
        'locked': false,
        'visible': true,
        'displayName': 'Exp. Type'
      },
      {
        'columnName': 'factors',
        'order': 7,
        'locked': false,
        'visible': true,
        'customComponent': DWHFileExpFactorCell,
        'displayName': 'Exp. Factors'
      },
      {
        'columnName': 'error_codes',
        'order': 8,
        'locked': false,
        'visible': true,
        'customComponent': DWHFileErrorCell,
        'displayName': 'Errors'
      }];
    return (
      <div>
      <h2> Files found in clone </h2>
      <div>
          {( !dwhfiles || dwhfiles.length === 0 ) ?
            <p> No files found </p>
            :
            <Griddle
            results={dwhfiles}
            tableClassName="table"
            showFilter
            showSettings
            columns={['status', 'name', 'path', 'organism', 'technology', 'experiment_type', 'factors', 'error_codes']}
            columnMetadata={columnMetadata}
            />
          }
      </div>
      </div>
    );
  }
}
