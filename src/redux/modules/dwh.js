const TRANSFER_FILE = 'redux-example/dwh/TRANSFER_FILE';
const TRANSFER_FILE_SUCCESS = 'redux-example/dwh/TRANSFER_FILE_SUCCESS';
const TRANSFER_FILE_FAIL = 'redux-example/dwh/TRANSFER_FILE_FAIL';

const initialState = {
  dwhfiles: [],
  dwhTransferError: null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case TRANSFER_FILE:
      return state; // 'saving' flag handled by redux-form
    case TRANSFER_FILE_SUCCESS:
      return {
        ...state,
        dwhfiles: action.result.dwhfiles
      };
    case TRANSFER_FILE_FAIL:
      return typeof action.error === 'string' ? {
        ...state,
        dwhTransferError: action.error
      } : state;
    default:
      return state;
  }
}

export function tranferFiles(data) {
  return {
    types: [TRANSFER_FILE, TRANSFER_FILE_SUCCESS, TRANSFER_FILE_FAIL],
    promise: (client) => client.post('/dwh/transferFiles', { data })
  };
}
