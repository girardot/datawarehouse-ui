export App from './App/App';
export Home from './Home/Home';
export About from './About/About';
export Login from './Login/Login';
export LoginSuccess from './LoginSuccess/LoginSuccess';
export TransferFiles from './TransferFiles/TransferFiles';
export NotFound from './NotFound/NotFound';
