import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {initialize} from 'redux-form';
import {TransferFileForm, TransferFileTable} from 'components';

@connect(
  state => ({
    dwhfiles: state.dwh.dwhfiles,
    dwhTransferError: state.dwh.dwhTransferError
  }),
  {initialize}
)
export default class TransferFiles extends Component {
  static propTypes = {
    initialize: PropTypes.func.isRequired,
    dwhfiles: PropTypes.array.isRequired,
    dwhTransferError: PropTypes.string
  }


  render() {
    const {dwhfiles, dwhTransferError} = this.props;
    console.log(dwhfiles);
    return (
      <div className="container">
        <h1>Transfer Files to the DataWareHouse</h1>
        <Helmet title="File transfer-files"/>
        {(dwhTransferError) ? <p> Error : {dwhTransferError} </p> : ''}

        <TransferFileTable dwhfiles={ dwhfiles } />

        <TransferFileForm filesAvailable={dwhfiles && dwhfiles.length > 0} />
      </div>
    );
  }
}
