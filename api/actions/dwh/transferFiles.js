var spawn = require('child_process').spawn;
var exec = require('child_process').exec;

const fakefiles_scan =
  [
    {
      'name': 'file1',
      'path': '/path/to/file1',
      'organism': 'dm',
      'technology': 'sequencing',
      'experiment_type': 'ChIP-seq',
      'factors': ['tissue', 'target', 'stage'],
      'factorvalues': ['we', 'Mef2', '6-8h'],
      'status': 'STATUS_TRANSFERABLE',
      'error_codes':[],
      'error_descriptions':[]
    },
    {
      'name': 'file2',
      'path': '/path/to/file2',
      'organism': 'dm',
      'technology': 'sequencing',
      'experiment_type': 'ChIP-seq',
      'factors': ['tissue', 'target', 'stage'],
      'factorvalues': ['we', 'Mef2', '8-10h'],
      'status': 'STATUS_ERROR',
      'error_codes':['ERROR_WRONG_LOCATION','ERROR_MISSING_META'],
      'error_descriptions':['file is misplaced','mandatory annotation non found : bandwidth']
    }
  ];

  const fakefiles_transfer =
    [
      {
        'name': 'file1',
        'path': '/path/to/file1',
        'organism': 'dm',
        'technology': 'sequencing',
        'experiment_type': 'ChIP-seq',
        'factors': ['tissue', 'target', 'stage'],
        'factorvalues': ['we', 'Mef2', '6-8h'],
        'status': 'STATUS_TRANSFERRED',
        'error_codes':[],
        'error_descriptions':[]
      },
      {
        'name': 'file2',
        'path': '/path/to/file2',
        'organism': 'dm',
        'technology': 'sequencing',
        'experiment_type': 'ChIP-seq',
        'factors': ['tissue', 'target', 'stage'],
        'factorvalues': ['we', 'Mef2', '8-10h'],
        'status': 'STATUS_ERROR',
        'error_codes':['ERROR_WRONG_LOCATION','ERROR_MISSING_META'],
        'error_descriptions':['file is misplaced','mandatory annotation non found : bandwidth']
      }
    ];


export default function transferFiles(req) {
  if( !req.session.user ){
    return Promise.reject({'error': 'How di dyou come here? You need to login first !'});
  }
  var user = req.session.user.name;
  console.log('user ' + user);
  var clonepath = req.body.clonepath;
  console.log('path of clone is ' + clonepath);
  var dryrun = req.body.dryrun;
  console.log('dry run ' + dryrun);
  var message = '';
  // init cmd with scan only options
  var cmd = 'echo scanning clone at path ' + clonepath + ' fake for user ' + user;
  if(!dryrun){
    // comamnd to transfer file from clone
    message = req.body.message;
    cmd = 'echo transfer files fake from clone at path ' + clonepath + 'with message ' + message;
  }

  return new Promise((resolve, reject) => {

    exec(cmd, function(error, stdout, stderr) {
      if (error) {
        reject({'error': error});
      } else {
        resolve({
          'dwhfiles': ( dryrun === "true" ? fakefiles_scan : fakefiles_transfer)
        });
      }
    });
  });
/*  var py = spawn('echo bonjour '+user);

	py.stdout.on('data', function(data){
		console.log(data.toString());
	});

	py.stderr.on('data', function (data) {
	  console.log('stderr: ' + data);
	});

	py.stdout.on('end', function(data){
		return Promise.resolve(data);
	});
*/
}
